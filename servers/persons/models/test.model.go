package models

import (
	"Go_Gingonic_Server/db"
)

type (
	Test struct {
		id               int     `json:"id"`
		test_description string  `json:"test_description"`
		quatity          int     `json:"quantity"`
		price            float64 `json:"price"`
		amount           float64 `json:"amount"`
		created_at       string  `json:"created_at"`
	}
)

type IModels interface {
	Get_all_test1()
}

type Base struct {
}

func (self *Base) Get_all_test1(query string) ([]map[string]interface{}, error) {

	con := db.Connect()

	defer con.Close()
	rows, err := con.Query(query)
	if err != nil {
		return nil, err
	}

	columns, err := rows.Columns()
	if err != nil {
		return nil, err
	}
	count := len(columns)
	tableData := make([]map[string]interface{}, 0)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		tableData = append(tableData, entry)
	}
	/* jsonData, err := json.Marshal(tableData) */
	if err != nil {
		return nil, err
	}

	return tableData, nil
}

/* func (self *Base) Get_all_test1() ([]Test, error) {
	con := db.Connect()

	sql := "select * from test"

	rs, erro := con.Query(sql)
	defer rs.Close()
	defer con.Close()
	if erro != nil {

		return nil, erro

	}

	var products []Test

	for rs.Next() {

		var product Test
		erro := rs.Scan(
			&product.id,
			&product.test_description,
			&product.quatity,
			&product.price,
			&product.amount,
			&product.created_at)

		if erro != nil {

			return nil, erro

		}

		products = append(products, product)

	}
	fmt.Print("Hola desde el modelo")
	fmt.Print(products)
	return products, nil
} */
