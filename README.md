# Golang-GinGonic API Rest Mysql 

Simple example of little Mysql api rest with golang gingonic

## Deploy the Environment
-   docker-compose --build

## How it works
```
    ├── servers
    ├──── persons   //the microservice prepared with the MySql API-REST
    ├────── main.go
    ├────── go.mod  //all dependencies needed by the example
    ├────── go.sum
    ├────── routers
        ├────── router.go      //business logic & router binding
    ├────── controllers
        ├────── controller.go  //put the before & after logic of handle request
    ├────── models
        ├────── models.go       //data models define & DB operation
    ├────── db
        ├────── utils.go        //small tools function
```

## Make a CRUD request

- Select
```
    http://{host}:8080/api/app/test

    [
        {
            "amount": "20.00",
            "created_at": "2019-11-14 11:13:19",
            "id": "1",
            "price": "2.00",
            "quatity": "10",
            "test_description": "test_oranges"
        }
    ]
```